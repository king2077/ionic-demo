import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NativeDemoPage } from './native-demo.page';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [NativeDemoPage],
    exports: [NativeDemoPage]
})
export class NativeDemoPageModule { }
