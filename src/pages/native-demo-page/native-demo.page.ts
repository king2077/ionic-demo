import { AlertController } from '@ionic/angular';
import { Component, NgZone } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CardIO } from '@ionic-native/card-io/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
    selector: 'app-native-demo-page',
    templateUrl: './native-demo.page.html',
    styleUrls: ['./native-demo.page.scss'],
})
export class NativeDemoPage {
    base64Image: string;
    cardInfo: any;
    appInfo: any;
    constructor(
        private camera: Camera,
        private cardIO: CardIO,
        private iab: InAppBrowser,
        private appVersion: AppVersion,
        private zone: NgZone,
        private alert: AlertController
    ) { }

    openCamera() {
        const options: CameraOptions = {
            quality: 100, // 照片质量
            destinationType: this.camera.DestinationType.DATA_URL, // 返回路径或base64
            encodingType: this.camera.EncodingType.JPEG, // 图片格式
            mediaType: this.camera.MediaType.PICTURE // 媒体类型（打开的媒体对象为照片）
        };
        this.camera.getPicture(options).then((imageData) => {
            // 得到base64格式的图片
            this.base64Image = 'data:image/jpeg;base64,' + imageData;
        }, (err) => {
            // 发生错误
            console.log(`open camera is error: ${err}`);
        });
    }

    async getCardInfo() {
        const result: boolean = await this.cardIO.canScan();
        if (result) {
            const options = {
                requireExpiry: true,
                requireCVV: false,
                requirePostalCode: false
            };

            this.cardInfo = await this.cardIO.scan(options);
            this.alertWindow('银行卡信息', `卡号:${this.cardInfo.cardNumber}<br>到期时间:${this.cardInfo.expiryYear}.${this.cardInfo.expiryMonth}`);
            console.log(this.cardInfo);
        }
    }

    openBaidu() {
        const browser = this.iab.create('https://baidu.com/');
    }

    async getVersion() {
        this.appInfo = {
            name: await this.appVersion.getAppName(),
            package: await this.appVersion.getPackageName(),
            versionCode: await this.appVersion.getVersionCode(),
            version: await this.appVersion.getVersionNumber()
        };
        this.alertWindow('APP信息', `名称:${this.appInfo.name}<br>包名:${this.appInfo.package}<br>版本代码:${this.appInfo.versionCode}<br>版本号：${this.appInfo.version}`);
    }

    async alertWindow(subtitle: string, msg: string) {
        const alert = await this.alert.create({
            header: '信息',
            subHeader: subtitle,
            message: msg,
            buttons: ['确定']
        });

        await alert.present();
    }

}
