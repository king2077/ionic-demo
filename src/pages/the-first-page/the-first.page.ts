import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-the-first-page',
    templateUrl: './the-first.page.html',
    styleUrls: ['./the-first.page.scss'],
})
export class TheFirstPage implements OnInit {
    @Input() name: string;

    constructor() { }

    ngOnInit() { }

}
