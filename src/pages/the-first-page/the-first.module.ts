import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TheFirstPage } from './the-first.page';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [TheFirstPage],
    exports: [TheFirstPage]
})
export class TheFirstPageModule { }
