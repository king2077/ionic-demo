import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Api } from 'src/services/api.service';

@Component({
    selector: 'app-http-demo-page',
    templateUrl: './http-demo.page.html',
    styleUrls: ['./http-demo.page.scss'],
})
export class HttpDemoPage {

    constructor(
        private api: Api,
        private toast: ToastController
    ) { }
    // 提示
    async toastShow(msg?: string, time?: number) {
        const toast = await this.toast.create({
            message: msg,
            duration: time ? time : 2000,
            position: 'bottom',
        });
        toast.present();
    }

    // 登录，采用传统的promise then
    login() {
        const params = {
            userName: 'Jack',
            password: '123456'
        };
        this.api.login(params).then((res: any) => {
            if (res.code === 1) {
                this.toastShow('登录成功');
            } else {
                this.toastShow('登录失败');
            }
        });
    }
    // 获取用户信息，采用传统的promise then
    getUserInfo() {
        const params = {
            userName: 'Jack'
        };
        this.api.getUserInfo(params).then((res: any) => {
            if (res.code === 1) {
                this.toastShow(JSON.stringify(res), 5000);
            } else {
                this.toastShow('获取失败');
            }
        });
    }
    // 更新，采用async await方式
    async updateUserInfo() {
        const params = {
            userName: 'Jack',
            password: '12345678'
        };
        const result: any = await this.api.updateUserInfo(params);
        if (result.code === 1) {
            this.toastShow(result.message);
        } else {
            this.toastShow(result.message);
        }
    }
    // 删除，采用async await方式
    async deleteUser() {
        const params = {
            id: 1
        };
        const result: any = await this.api.deleteUser(params);
        if (result.code === 1) {
            this.toastShow(result.message);
            const a: any = {};
            console.log(a[0].name);
        } else {
            this.toastShow(result.message);
        }
    }

}
