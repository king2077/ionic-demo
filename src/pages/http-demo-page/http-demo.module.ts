import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HttpDemoPage } from './http-demo.page';

@NgModule({
    imports: [CommonModule, FormsModule, IonicModule],
    declarations: [HttpDemoPage],
    exports: [HttpDemoPage]
})
export class HttpDemoPageModule { }
