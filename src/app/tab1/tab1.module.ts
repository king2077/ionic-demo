import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { TheFirstPageModule } from '../../pages/the-first-page/the-first.module';

import { Tab1PageRoutingModule } from './tab1-routing.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        TheFirstPageModule,
        Tab1PageRoutingModule
    ],
    declarations: [Tab1Page]
})
export class Tab1PageModule { }
