import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform } from '@ionic/angular';

@Component({
    selector: 'app-tabs',
    templateUrl: 'tabs.page.html',
    styleUrls: ['tabs.page.scss']
})
export class TabsPage {

    constructor(
        private statusbar: StatusBar,
        private platform: Platform
    ) {
        this.platform.ready().then(() => {
            this.statusbar.backgroundColorByHexString('#DA4136');
        });
    }

}
