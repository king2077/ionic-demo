import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-tab3',
    templateUrl: 'tab3.page.html',
    styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

    constructor(private statusbar: StatusBar) {

    }

    ionViewWillEnter() {
        this.statusbar.backgroundColorByHexString('#FFCC33');
    }
}
