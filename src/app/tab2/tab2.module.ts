import { HttpDemoPageModule } from './../../pages/http-demo-page/http-demo.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { TheFirstPageModule } from '../../pages/the-first-page/the-first.module';

import { Tab2PageRoutingModule } from './tab2-routing.module';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        TheFirstPageModule,
        Tab2PageRoutingModule,
        HttpDemoPageModule
    ],
    declarations: [Tab2Page]
})
export class Tab2PageModule { }
