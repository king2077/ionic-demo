import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

    constructor(private statusbar: StatusBar) {

    }

    ionViewWillEnter() {
        this.statusbar.backgroundColorByHexString('#FF33CC');
    }
}
