import { GlobalErrorHandlerService } from './../services/global-error-handler.service';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// 注入服务
import { Api } from 'src/services/api.service';
import { HttpInterceptorService } from './../services/http-interceptor.service';

// 注入原生插件
import { Camera } from '@ionic-native/camera/ngx';
import { CardIO } from '@ionic-native/card-io/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule, HttpClientModule, IonicModule.forRoot({
        backButtonIcon: 'ios-arrow-back', // 返回按钮的图标
        backButtonText: '', // 返回按钮文字，留空
        mode: 'ios', // 采用ios设计模式
    }), AppRoutingModule],
    providers: [
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
        { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
        Api, // 注入Api服务
        Camera, // 注入原生摄像头调用服务
        CardIO, // 扫描银行卡信息
        InAppBrowser, // 在app内打开外部连接
        AppVersion,  // 获取当前App版本信息
        StatusBar, // 状态栏
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
