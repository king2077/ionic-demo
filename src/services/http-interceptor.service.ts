import { ToastController } from '@ionic/angular';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
        private router: Router,
        private toast: ToastController
    ) { }

    // 全局http拦截器，所有http请求都会经过这个拦截器
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = 'my-token-string-from-server';
        // 设置token
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: token
                }
            });
        }
        // 设置http请求头
        if (!request.headers.has('Content-Type')) {
            request = request.clone({
                setHeaders: {
                    'content-type': 'application/json'
                }
            });
        }
        request = request.clone({
            headers: request.headers.set('Accept', 'application/json')
        });
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event--->>>', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    if (error.error.success === false) {
                        this.presentToast('Login failed');
                    } else {
                        // 如果未登录，则直接返回登录界面
                        this.router.navigate(['login']);
                    }
                }
                return throwError(error);
            }));
    }

    async presentToast(msg: any) {
        const toast = await this.toast.create({
            message: msg,
            duration: 2000,
            position: 'top'
        });
        toast.present();
    }
}
