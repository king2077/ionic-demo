import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class Api {
    baseUrl: String;
    constructor(private http: HttpClient) {
        this.baseUrl = environment.baseUrl;
    }

    /**
     *
     * 封装http请求
     * @param {*} url api接口
     * @param {*} method 请求方式get/post/put/delete
     * @param {*} body 请求参数
     * @memberof Api
     */
    httpRequest(url, method, body) {
        return new Promise((resolve, reject) => {
            const requestUrl = this.baseUrl + url;
            // get请求方式
            if (method === 'get') {
                this.http.get(requestUrl, { params: body }).subscribe((data: any) => {
                    if (data) {
                        resolve(data);
                    } else {
                        // 自己定义错误信息
                        resolve({ code: -1, msg: data.msg });
                    }
                }, err => {
                    reject({ code: -1, err });
                });
            }
            // post请求方式
            if (method === 'post') {
                // 如不需要序列化参数，可以去掉this.serialize()方法
                this.http.post(requestUrl, body).subscribe((data: any) => {
                    if (data) {
                        resolve(data);
                    } else {
                        resolve({ code: -1, msg: data.msg });
                    }
                }, err => {
                    reject({ code: -1, err });
                });
            }
            // put请求方式
            if (method === 'put') {
                this.http.put(requestUrl, body).subscribe((data: any) => {
                    if (data) {
                        resolve(data);
                    } else {
                        resolve({ code: -1, msg: data.msg });
                    }
                }, err => {
                    reject({ code: -1, err });
                });
            }
            // delete请求
            if (method === 'delete') {
                this.http.delete(requestUrl, { params: body }).subscribe((data: any) => {
                    if (data) {
                        resolve(data);
                    } else {
                        // 自己定义错误信息
                        resolve({ code: -1, msg: data.msg });
                    }
                }, err => {
                    reject({ code: -1, err });
                });
            }
        });
    }

    // 登录接口
    async login(body: any) {
        return await this.httpRequest('/login', 'post', body);
    }

    // 获取用户信息
    async getUserInfo(body: any) {
        return await this.httpRequest('/getUserInfo', 'get', body);
    }

    // 修改用户信息
    async updateUserInfo(body: any) {
        return await this.httpRequest('/update', 'put', body);
    }

    // 修改用户信息
    async deleteUser(body: any) {
        return await this.httpRequest('/delete', 'delete', body);
    }
}
