import { Injectable, ErrorHandler, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
// import { NGXLogger } from 'ngx-logger';
import * as StackTrace from 'stacktrace-js';

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {
    constructor(private injector: Injector) { }
    handleError(error) {
        // const loggingService = this.injector.get(NGXLogger);
        const location = this.injector.get(LocationStrategy);
        const message = error.message ? error.message : error.toString();
        const url = location instanceof PathLocationStrategy ? location.path() : '';
        StackTrace.fromError(error).then(stackframes => {
            const stackString = stackframes
                .splice(0, 20)
                .map((sf) => {
                    return sf.toString();
                })
                .join('\n');
            console.log('stackString', { message, stack: stackString });
            // loggingService.error('error', {message, stack: stackString});
        });
        throw error;
    }
}
